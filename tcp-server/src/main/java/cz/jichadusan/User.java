package cz.jichadusan;

/**
 * Created with IntelliJ IDEA.
 * User: jichadusan
 * Date: 27.8.13
 * Time: 20:45
 * To change this template use File | Settings | File Templates.
 */
public class User {
    private String  userName;
    private boolean loggedOn = false;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public User witUserName(String userName) {
        setUserName(userName);
        return this;
    }

    public boolean isLoggedOn() {
        return loggedOn;
    }

    public void setLoggedOn(boolean loggedOn) {
        this.loggedOn = loggedOn;
    }
}
