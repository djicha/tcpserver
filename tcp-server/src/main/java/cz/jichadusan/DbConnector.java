package cz.jichadusan;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: i18n
 * Date: 27.8.13
 * Time: 15:38
 * Load db configuration and create connection to postgresql db
 */
public class DbConnector {
    private Connection   connection = null;
    private SQLException dbError = null;

    /**
     * Properties props = new Properties();
     * props.setProperty("user","cma");
     * props.setProperty("password","cma");
     * props.setProperty("ssl","true");
     * @throws SQLException
     */
    public DbConnector() {
        try {
            Class.forName("org.postgresql.Driver");
        }   catch (Exception e) {
           e.printStackTrace();
        }
        String url            = Helper.getResourceString("db.urlBase")+Helper.getResourceString("db.dbName");
        try {
            this.connection   = DriverManager.getConnection(url, getConnectionArguments());
        } catch (SQLException ext) {
            connection = null;
            dbError = ext;
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public SQLException getDbError() {
        return dbError;
    }

    /**
     * Possible override in child class
     * @return
     */
    protected Properties getConnectionArguments() {
        Properties properties = new Properties();
        properties.setProperty("user",     Helper.getResourceString("db.user"));
        properties.setProperty("password", Helper.getResourceString("db.password"));
        return properties;
    }

}
