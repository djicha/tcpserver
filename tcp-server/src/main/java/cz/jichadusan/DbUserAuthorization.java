package cz.jichadusan;

import com.google.common.base.Throwables;
import com.google.common.collect.Maps;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: jichadusan
 * Date: 27.8.13
 * Time: 20:41
 * User authorization over DB, increments ok and bad logins for existing user name
 */
public class DbUserAuthorization {

    private Connection connection;

    public DbUserAuthorization(DbConnector connector) {
        this.connection = connector != null ? connector.getConnection() : null;
    }

    public User authorizeUser(User user, String password) {
        //it is safer to use prepared statements (possible SQL injection)
        final String sql = "select count(\"id\") pocticek from users where \"userName\" = ? and \"password\" = md5(?)";

        try {
            PreparedStatement ps = connection != null ? connection.prepareStatement(sql) : null;
            ps.setString(1, user.getUserName());
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            rs.next();
            int loggedStatus = rs.getInt("pocticek");
            if (loggedStatus >= 1) {
                user.setLoggedOn(true);
            } else {
                user.setLoggedOn(false);
            }
            updateLogonResultCounter(user);

        } catch (SQLException e) {
            Logger.getAnonymousLogger().warning(e.getMessage());
            Throwables.propagate(e);
        }

        return user;
    }

    private void updateLogonResultCounter(User user) {
        final String oKKo = user.isLoggedOn() ? "logOK" : "logKO";
        final String updateSqlString = "update users set \""+oKKo+"\" = (select (u.\""+oKKo+"\"+1) pocticek from \"users\" u where u.\"userName\" = ?) where \"userName\" = ?";

        try {
            PreparedStatement ps = connection != null ? connection.prepareStatement(updateSqlString) : null;
            ps.setString(1, user.getUserName());
            ps.setString(2, user.getUserName());
            int res = ps.executeUpdate();
        } catch (SQLException e) {
            Logger.getAnonymousLogger().warning(e.getMessage());
            Throwables.propagate(e);
        }
    }

    public Map<LogonResult, Integer> getLogonStats(User user) {
        Map<LogonResult, Integer> stats = Maps.newHashMap();

        final String statsSql = "select u.\"logOK\" ok, u.\"logKO\" ko from \"users\" u where u.\"userName\" = ?";
        try {
            PreparedStatement ps = connection != null ? connection.prepareStatement(statsSql) : null;
            ps.setString(1, user.getUserName());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
            stats.put(LogonResult.OK, rs.getInt("ok"));
            stats.put(LogonResult.KO, rs.getInt("ko"));
            }
            return stats;
        } catch (SQLException e) {
            Logger.getAnonymousLogger().warning(e.getMessage());
            Throwables.propagate(e);
        }
        return null;
    }

    public enum LogonResult {
        OK,
        KO
    }
}
