package cz.jichadusan;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Created with IntelliJ IDEA.
 * User: i18n
 * Date: 27.8.13
 * Time: 14:26
 * To change this template use File | Settings | File Templates.
 */
public class Helper {
    private static ResourceBundle resourceBundle = null;

    /**
     * Checks to see if a specific port is available.
     *
     * @param port the port to check for availability
     * @see "Apache camel project", funcion is from this project
     * inspired by http://stackoverflow.com/questions/434718/sockets-discover-port-availability-using-java
     */
    public static boolean isPortAvailable(int port) {
        final int MAX_PORT_NUMBER = 65535;
        final int MIN_PORT_NUMBER = 0;

        if (port < MIN_PORT_NUMBER || port > MAX_PORT_NUMBER) {
            throw new IllegalArgumentException("Invalid start port: " + port);
        }

        ServerSocket ss = null;
        DatagramSocket ds = null;
        try {
            ss = new ServerSocket(port);
            ss.setReuseAddress(true);
            ds = new DatagramSocket(port);
            ds.setReuseAddress(true);
            return true;
        } catch (IOException e) {
        } finally {
            if (ds != null) {
                ds.close();
            }

            if (ss != null) {
                try {
                    ss.close();
                } catch (IOException e) {
                /* should not be thrown */
                }
            }
        }

        return false;
    }

    public static String getResourceString(String keyProperty) {
        if (resourceBundle == null) {
            resourceBundle = ResourceBundle.getBundle("i18n.server", new Locale("cs"));
        }
        String property = resourceBundle.getString(keyProperty);

        return property;
    }
}