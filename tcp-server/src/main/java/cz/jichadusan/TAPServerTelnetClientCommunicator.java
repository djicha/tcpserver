package cz.jichadusan;

import com.google.common.collect.ImmutableSet;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: jichadusan
 * Date: 27.8.13
 * Time: 22:04
 * Data interchange between client and server
 */
public class TAPServerTelnetClientCommunicator extends Thread {
    static final Set<String> EXIT_COMMANDS = ImmutableSet.of("quit","exit");
    static final String help = Helper.getResourceString("app.message.help");
    static final String userNameLabel = Helper.getResourceString("app.message.username");
    static final String passwordLabel = Helper.getResourceString("app.message.password");
    static final String OK = Helper.getResourceString("app.count.OK");
    static final String KO = Helper.getResourceString("app.count.KO");

    Socket clientSocket;
    DbUserAuthorization dbUserAuthorization;
    DataInputStream dataInputStream;
    DataOutputStream dataOutputStream;
    BufferedReader inputClientReader;
    String userName;
    String password;

    TAPServerTelnetClientCommunicator(Socket socket, DbUserAuthorization userAuthorization) throws Exception {
        clientSocket = socket;
        this.dbUserAuthorization = userAuthorization;
        System.out.println("Client Connected ...");
        dataInputStream = new DataInputStream(clientSocket.getInputStream());
        inputClientReader = new BufferedReader(new InputStreamReader( dataInputStream, Charset.forName("windows-1250")));
        dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());
        start();
    }
    public void run() {
        try {
            System.out.println("Waiting for UserName And password");
            boolean run;
            dataOutputStream.writeChars(userNameLabel + "\r\n");
            dataOutputStream.flush();
            userName = inputClientReader.readLine();

            dataOutputStream.writeChars(passwordLabel + "\r\n");
            dataOutputStream.flush();
            password = inputClientReader.readLine();

            User user = new User().witUserName(userName);
            user = dbUserAuthorization.authorizeUser(user, password);

            run = user.isLoggedOn();
            Map<DbUserAuthorization.LogonResult, Integer> stats = dbUserAuthorization.getLogonStats(user);
            final String stringStats = !stats.isEmpty() ? (OK+" "+(stats != null ? stats.get(DbUserAuthorization.LogonResult.OK)+" " : " "))+(KO+" "+(stats != null ? stats.get(DbUserAuthorization.LogonResult.KO) : " ")) : Helper.getResourceString("app.message.unknowUser");
            final String status = (run ? "OK, " : "Chyba, ") + stringStats + "\r\n";
            System.out.println(status);
            dataOutputStream.writeChars(status);
            dataOutputStream.writeChars(help);
            dataOutputStream.flush();

            // after login to client
            while(run)
            {
                String strCommand = inputClientReader.readLine();
                if(EXIT_COMMANDS.contains(strCommand != null ?  strCommand.toLowerCase() : null)) {
                    run=false;
                } else {
                    dataOutputStream.writeChars(help);
                    dataOutputStream.flush();
                }
            }
            clientSocket.close();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }
}
