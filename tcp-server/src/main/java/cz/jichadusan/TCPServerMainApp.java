package cz.jichadusan;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

public class TCPServerMainApp {

    final static TCPServerPortValidator validator = new TCPServerPortValidator();

    public static void main(String[] args) throws Exception {

        String applicationPort;
        try{
            String portFromParams = parsePortFromaApplicationArgs(args);
            if (validator.withTcpPort(portFromParams).getPortValidationResult() != TCPServerPortValidator.PortValidationResult.OK) {
                validator.printValidationResult();
                System.out.println(Helper.getResourceString("app.message.input"));
                BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
                applicationPort = bufferRead.readLine();
            } else {
                applicationPort = portFromParams;
            }

            if (TCPServerPortValidator.PortValidationResult.OK != validator.withTcpPort(applicationPort).getPortValidationResult()) {
                validator.printValidationResult();
                System.out.println(Helper.getResourceString("app.message.bye"));
                System.in.read();
                System.exit(0); // bad port number, or used port --- end application
            }

            // after port validation, try db connect
            DbConnector connector = new DbConnector();
            if (connector.getConnection() == null) {
                connector.getDbError().printStackTrace();
                System.out.println(Helper.getResourceString("app.message.bye"));
                System.in.read();
                System.exit(0);

            }

            //after db connect try listen on TCP port
            int port = validator.getPortNumber().intValue();
            ServerSocket serverSocket = new ServerSocket(port);
            if (serverSocket != null) {
                System.out.println("server run and listen on tcp port "+port+"...");
            }

            while(true)
            {
                Socket soc = serverSocket.accept();
                TAPServerTelnetClientCommunicator acceptTelnetClient = new TAPServerTelnetClientCommunicator(soc, new DbUserAuthorization(connector));
            }


        }
        catch(IOException e)
        {
            e.printStackTrace();
        }

    }

    /**
     * Ziska z argumentu aplikace port tcp/udp ze tvaru "port:8080"
     * @param args
     * @return
     */
    private static String parsePortFromaApplicationArgs(String[] args) {

        if (args.length >= 1) {
            String portParam = args[0];
            List<String> portSplit = Lists.newArrayList(Splitter.on(':').omitEmptyStrings().trimResults().split(portParam));
            return portSplit != null && portSplit.size() == 2 ? portSplit.get(1) : null;
        }

        return null;
    }

}
