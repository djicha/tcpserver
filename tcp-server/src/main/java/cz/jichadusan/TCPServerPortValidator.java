package cz.jichadusan;

import com.google.common.base.Strings;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: i18n
 * Date: 27.8.13
 * Time: 6:21
 * To change this template use File | Settings | File Templates.
 */
public class TCPServerPortValidator {

    private String tcpPort;

    private PortValidationResult portValidationResult;

    public TCPServerPortValidator() {
    }

    public TCPServerPortValidator(String tcpPort) {
        this.setTcpPort(tcpPort);
    }

    private void validatePort() {
        if (Strings.isNullOrEmpty(this.tcpPort)) {
            setPortValidationResult(PortValidationResult.EMPTY);
            return;
        }

        if (!validateNumber(this.tcpPort)) {
            setPortValidationResult(PortValidationResult.WRONG_NUMBER_VALUE);
            return;
        }

        if (!Helper.isPortAvailable(new BigDecimal(this.tcpPort).intValue())) {
            setPortValidationResult(PortValidationResult.PORT_NOT_AVAILABLE);
            return;
        }
        setPortValidationResult(PortValidationResult.OK);
    }

    public PortValidationResult getPortValidationResult() {
        return portValidationResult;
    }

    public void setPortValidationResult(PortValidationResult portValidationResult) {
        this.portValidationResult = portValidationResult;
    }

    public void printValidationResult() {
        PortValidationResult portValidationResult1 = getPortValidationResult();
        System.out.println(portValidationResult1 != null ? portValidationResult1.getResultMessage() : "Not validated!");
    }

    public BigDecimal getPortNumber() {
       return new BigDecimal(tcpPort);
    }

    private void setTcpPort(String tcpPort) {
        this.tcpPort = tcpPort;
        validatePort();
    }

    public TCPServerPortValidator withTcpPort(String tcpPort) {
        this.setTcpPort(tcpPort);
        return this;
    }

    private boolean validateNumber(String input) {
        try {
            BigDecimal number = new BigDecimal(input);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public enum PortValidationResult {
        OK {
            @Override
            public String getResultMessage() {
                return "OK";
            }
        },
        EMPTY {
            @Override
            public String getResultMessage() {
                return "Port value - empty!";
            }
        },
        WRONG_NUMBER_VALUE {
            @Override
            public String getResultMessage() {
                return "Port value - wrong number format!";
            }
        },
        PORT_NOT_AVAILABLE {
            @Override
            public String getResultMessage() {
                return "Port value - is used by another computer process";
            }
        }
        ;

        public abstract String getResultMessage();
    }
}
