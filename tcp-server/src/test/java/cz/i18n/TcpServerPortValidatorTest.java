package cz.i18n;

import cz.jichadusan.TCPServerPortValidator;
import org.junit.Test;

import java.util.logging.Logger;

import static junit.framework.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: i18n
 * Date: 27.8.13
 * Time: 7:33
 * To change this template use File | Settings | File Templates.
 */

public class TcpServerPortValidatorTest  {

    Logger logger = Logger.getAnonymousLogger();

    @Test
    public void tcpServerPortValidatorTest() {
        logger.info("Test OK");
        TCPServerPortValidator validator = new TCPServerPortValidator().withTcpPort("12345");
        assertEquals(TCPServerPortValidator.PortValidationResult.OK, validator.getPortValidationResult());
        validator.printValidationResult();

        logger.info("Test empty");
        validator.withTcpPort("");
        assertEquals(TCPServerPortValidator.PortValidationResult.EMPTY, validator.getPortValidationResult());
        validator.printValidationResult();

        logger.info("Test bad number");
        validator.withTcpPort("hula-hula-hop");
        assertEquals(TCPServerPortValidator.PortValidationResult.WRONG_NUMBER_VALUE, validator.getPortValidationResult());
        validator.printValidationResult();

        logger.info("Test used port");
        validator.withTcpPort("80");        // for success, must port number 80 used by another computer process (apache tomcat, skype etc.)
        assertEquals(TCPServerPortValidator.PortValidationResult.PORT_NOT_AVAILABLE, validator.getPortValidationResult());
        validator.printValidationResult();
    }
}
