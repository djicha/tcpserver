CREATE TABLE users
(
  id integer NOT NULL,
  "userName" text,
  password text,
  "logOK" integer DEFAULT 0,
  "logKO" integer DEFAULT 0,
  CONSTRAINT id PRIMARY KEY (id),
  CONSTRAINT username UNIQUE ("userName")
)
WITH (
  OIDS=FALSE
);